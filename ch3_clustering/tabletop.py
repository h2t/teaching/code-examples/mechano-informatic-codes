import numpy as np


def generate_tabletop_scene(n_samples=80):
    plate_points = np.random.default_rng().multivariate_normal(
        mean=[50, 30],
        cov=[[6, -0.7],
             [-0.7, 3]],
        size=n_samples
    )
    cup_points_1 = np.random.default_rng().multivariate_normal(
        mean=[25, 50],
        cov=[[10, 8],
             [8, 15]],
        size=int(n_samples / 2 + 15)
    )
    cup_points_2 = np.random.default_rng().multivariate_normal(
        mean=[70, 55],
        cov=[[9, -0.9],
             [-0.9, 20]],
        size=int(n_samples / 2 - 15)
    )
    fork_points = np.random.default_rng().multivariate_normal(
        mean=[80, 27],
        cov=[[8, 0],
             [0, 3]],
        size=n_samples
    )

    tabletop_points = np.vstack([plate_points, cup_points_1, cup_points_2, fork_points])
    tabletop_labels = np.array(["plate"] * 80 + ["cup"] * 80 + ["fork"] * 80)

    return tabletop_points, tabletop_labels
