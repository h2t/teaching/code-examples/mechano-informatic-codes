import numpy as np
import time
from scipy.optimize import minimize, Bounds

from kukaEnv import KukaGripperEnv
import controllers
import kuka
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Number of iterations
    nb_iters = 250
    # Create environment
    env = KukaGripperEnv(block_pos=[.65, 0.12, -0.15, np.pi/2])

    # Create joint position controller
    ctrl = controllers.SimpleJointPController(kp=.01)

    # Wait that the environment is started
    while not env.started():
        env.move_camera()
        time.sleep(env._timeStep)

    # Define target position
    target_position = [.65, 0.1, 0.1, 0]

    # Log
    log_joint_angles = np.zeros((nb_iters, 7))
    log_joint_velocities = np.zeros((nb_iters, 7))
    log_position = np.zeros((nb_iters, 3))
    log_function = np.zeros(nb_iters)


    # Main loop
    for i in range(nb_iters):
        # Get current state
        joint_angles = env.get_state(to_ctrl=kuka.JOINT)
        position = env.get_state(to_ctrl=kuka.TCP)  # End-effector position

        # Desired velocity in function of the target position
        desired_velocity = 5e+3 * np.array(target_position - position)[:3]
        # Current position jacobian of the robot
        jac_trans, jac_rot = env.compute_jacobian()
        jac_trans = np.array(jac_trans)

        # Define the optimization function
        # 1. Find dq such that ||dx - Jdq || is minimized
        def f1(dq):
            return np.linalg.norm(desired_velocity-np.dot(jac_trans, dq))

        def f1_derivative(dq):
            return -2 * np.dot(jac_trans.T, desired_velocity-np.dot(jac_trans, dq))

        # 2. Find dq such that ||dx - Jdq || is minimized and such that the motion of the 4th joint is penalized
        def f2(dq):
            return np.linalg.norm(desired_velocity-np.dot(jac_trans, dq)) + dq[4]**2

        # 3. Find dq such that ||dx - Jdq || is minimized and such that high velocities are penalized
        def f3(dq):
            weights = 0.001 * np.diag([1., 1., 1., 1., 1., 1., 1.])
            return np.linalg.norm(desired_velocity-np.dot(jac_trans, dq)) + np.dot(dq.T, np.dot(weights, dq))

        # 4. Find dq such that ||dx - Jdq || is minimized and such that the motion of the 3 first joints is penalized
        # 3x more than the motion of the 4 last joints
        def f4(dq):
            weights = 0.0001 * np.diag([3., 3., 3., 1., 1., 1., 1.])
            return np.linalg.norm(desired_velocity-np.dot(jac_trans, dq)) + np.dot(dq.T, np.dot(weights, dq))

        # 2. Find dq such that ||dx - Jdq || is minimized along x and y
        def f5(dq):
            error = desired_velocity-np.dot(jac_trans, dq)
            weights = np.eye(3)
            weights[2, 2] = 0.0
            return np.sqrt(np.dot(error.T, np.dot(weights, error)))

        # 6. Find dq such that ||dx - Jdq || is minimized and such that the orientation change is penalized
        def f6(dq):
            return np.linalg.norm(desired_velocity-np.dot(jac_trans, dq)) + np.linalg.norm(np.dot(jac_rot, dq))**2

        # Optimization
        # Starting point
        dq0 = np.zeros(7)

        # Optimize function with scipy minimize
        # 1. Optimize the function using the Newton conjugate gradient method.
        # Note: the derivative of the function must be provided.
        res = minimize(f1, dq0, method='Newton-CG', jac=f1_derivative, options={'disp': False})
        # 2. Optimize the function using the BFGS method. The derivative of the function does not need to be provided.
        # res = minimize(f1, x0, method='BFGS', options={'disp': False})
        # 3. Optimize the function with additional bounds on the joint velocities.
        # Note: not all the optimization method support bound constraints, so that you should choose an adapted one.
        # bounds = Bounds(-500 * np.ones(7), 500 * np.ones(7))
        # res = minimize(f1, x0, method='SLSQP', bounds=bounds, options={'disp': False})

        # Results: joint velocities
        joint_velocities = res.x
        # Print result and function value
        print('The minimum value of the function {} is achieved for dq {}'.format(res.fun, res.x))

        # Define desired joint position by integrating the desired joint velocity
        target_state = joint_angles + joint_velocities * env._timeStep

        # Set controller, run one simulation step
        ctrl.set_target(target_state)
        env.run_controller_1step(ctrl)

        # Logs
        log_joint_angles[i] = joint_angles
        log_joint_velocities[i] = joint_velocities
        log_position[i] = position[:3]
        log_function[i] = res.fun

    env.close_gripper()
    # Final plots
    plt.rcParams.update({'font.size': 10})
    # Desired vs current position
    fig1 = plt.figure(1, figsize=(10, 10))
    for i in range(3):
        plt.subplot(3, 1, i + 1)
        plt.hlines(target_position[i], 0, nb_iters, color='r', linewidth=3.)
        plt.plot(log_position[:, i], color='skyblue', linewidth=3.)
        plt.xlabel(r'Iteration', fontsize=20)
        plt.ylabel(r'$x_{}$'.format(i), fontsize=20)
        plt.subplots_adjust(hspace=0.5)

    # Joint angles
    fig2 = plt.figure(2, figsize=(10, 10))
    for i in range(6):
        ax = plt.subplot(4, 2, i + 1)
        plt.plot(log_joint_angles[:, i], color='skyblue', linewidth=3.)
        plt.xlabel(r'Iteration', fontsize=20)
        plt.ylabel(r'$q_{}$'.format(i), fontsize=20)
        plt.subplots_adjust(hspace=0.5)
        plt.subplots_adjust(wspace=0.3)

    # Joint velocities
    fig3 = plt.figure(3, figsize=(10, 10))
    for i in range(6):
        plt.subplot(4, 2, i + 1)
        plt.plot(log_joint_velocities[:, i]/100, color='skyblue', linewidth=3.)
        plt.xlabel(r'Iteration', fontsize=20)
        plt.ylabel(r'$dq_{} (x100)$'.format(i), fontsize=20)
        plt.subplots_adjust(hspace=0.5)
        plt.subplots_adjust(wspace=0.3)

    # Function evolution
    # fig4 = plt.figure(1, figsize=(10, 10))
    # plt.plot(log_function, color='g', linewidth=3.)
    # plt.xlabel(r'Iteration', fontsize=28)
    # plt.ylabel(r'Function value'.format(i), fontsize=28)
    plt.show()
