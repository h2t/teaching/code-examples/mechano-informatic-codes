from kukaEnv import KukaGripperEnv
import controllers

def run_simple_pospctrl():
    env = KukaGripperEnv()
    ctrl = controllers.SimpleTCPPController(kp=.1)
    target_state = env.get_state()
    print(target_state)
    target_state[2] += 1
    ctrl.set_target(target=target_state)
    env.run_controller(ctrl)

def run_simple_traj_tracker():
    env = KukaGripperEnv()
    env.run_controller(controllers.SimpleTrajTracker(kp=.1))

def run_simple_jointpctrl():
    env = KukaGripperEnv()
    ctrl = controllers.SimpleJointPController(kp=.01)
    target_state = env.get_state(to_ctrl=ctrl.to_ctrl)
    print(target_state)
    target_state[5] += 1
    ctrl.set_target(target=target_state)
    env.run_controller(ctrl)


if __name__ == '__main__':
    run_simple_jointpctrl()

