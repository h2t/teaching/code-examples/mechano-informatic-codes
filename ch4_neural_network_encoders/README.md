# Neural Network Encoders

These examples show 
Auto-Encoders (AE),
Variational Auto-Encoders (VAE)
and Self-Organizing Maps (SOM).


## Running the Examples

You can set up the virtual environment as with all other examples.

To run an example, activate the environment 
and just call the script in the command line:

```bash
cd path/to/ch4_neural_network_encoders
source venv/bin/activate

python auto_encoder.py
python self_organizing_map.py
python variational_auto_encoder.py
```

