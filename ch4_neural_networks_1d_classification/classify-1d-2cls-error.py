#! /usr/bin/env python3

import os
import numpy as np
import matplotlib.pyplot as plt

from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model


def make_targets(X) -> np.ndarray:
    # Create labels for input data.
    targets = []
    for x in X:
        # Assign a random label with small probability.
        if np.random.random() < 0.1:
            samples = [[0, 1], [1, 0]]
            targets.append(samples[np.random.choice(2)])
        # Assign wrong label in a small interval.
        elif 0.5 < x < 0.6:
            targets.append([1, 0])
        elif x < 0:
            targets.append([1, 0])
        else:
            targets.append([0, 1])

    return np.array(targets)


def make_model() -> Model:
    # 1D input.
    inputs = Input(shape=(1,))

    # Two hidden layers with 10 neurons each.
    output_1 = Dense(10, activation='relu')(inputs)
    output_2 = Dense(10, activation='relu')(output_1)
    # Output is two class probabilities.
    predictions = Dense(2, activation='softmax')(output_2)

    # Create and compile the model.
    model = Model(inputs=inputs, outputs=predictions)
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model


if __name__ == "__main__":

    # Create training and test data.
    X_train = np.random.uniform(-1, 1, 1000)
    Y_train = make_targets(X_train)

    X_test = np.linspace(-1, 1, 200)
    Y_test = make_targets(X_test)

    # Create the model.
    model = make_model()

    # Run training and visualize performance.
    num_epochs = 500
    num_epochs_per_round = 5
    num_rounds = int(np.ceil(num_epochs / num_epochs_per_round))

    png_path_fmt = "plots/classify-1d-2cls-error/round-{}.png"
    os.makedirs(os.path.dirname(png_path_fmt.format(0)), exist_ok=True)

    for round in range(num_rounds):
        # Run current model on test data.
        Y_pred = model.predict(X_test)

        # Plot model's current performance.
        fig = plt.figure(dpi=300)
        plt.scatter(X_test, Y_pred[:, 1], s=8, label="Prediction")
        plt.scatter(X_test, Y_test[:, 1], s=4, label="Test Data")
        plt.legend(loc="upper left")
        plt.xlabel("$x$")
        plt.ylabel("$p(y = 1)$")
        plt.title("After {} epochs".format(round * num_epochs_per_round))
        plt.tight_layout()
        plt.savefig(png_path_fmt.format(round))
        plt.clf()
        del fig

        # Train the model.
        model.fit(X_train, Y_train, epochs=num_epochs_per_round)



